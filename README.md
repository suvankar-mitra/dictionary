# dictionary

<br>I am developing this as a hobby project to learn more about `Java Spring boot` concepts, as well as mobile development with `Flutter`.
<br>This project contains two sub projects.

## 1. [dictionary-api](dictionary-api/) (Java)

This is a Java Spring boot based REST API which word as a backend of the "dictionary" application as a whole.

## 2. [wordef](wordef/) (Flutter)

This is the frontend application built with Flutter that will use the above REST API to fetch data and then show in the UI.
