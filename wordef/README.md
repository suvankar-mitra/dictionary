# wordef - Dictionary UI

Dictionary UI application built with `Flutter`.

## Screenshots

<img src="screenshot/1_home.png" height=200> <img src="screenshot/2_home.png" height=200> <img src="screenshot/3_definition.png" height=200>
<br>
<img src="screenshot/1_home_dark.png" height=200> <img src="screenshot/2_home_dark.png" height=200> <img src="screenshot/3_definition_dark.png" height=200>

## Features (implemented):

- Simple, clean and intuitive UI UX

### Home screen

- Search box at the top where you can search any English word or a phrase
- UI supports Light and Dark theme (currently controlled by the system theme mode)
- Search box has an indicator for which theme is active right now (Light/dark)
- A random word will appear in the home screen inside a card
  - Random word card will change every 30 sec
  - There will be a play/pause button for the random card change - it can play/pause the random word generation
  - If the random card is out of visibility, the timer will pause (this will reduce API call to backend)
- A recent search section will appear below random card, we can see up to last 20 recent searches
- At the bottom there will be a button that will redirect to this git project link
  - is this a good idea?

### Definition screen

- At the top the searched word/phrase will appear in large font
- up to 2 phonetics will be shown below the word
- If a pronunciation is available then a speaker button will appear, which is clickable - once clicked the pronunciation can be heard
- Next a card will be shown for definitions

  - Next to the DEFINITION header, a number of 'total available definitions' will be shown
  - Definition card will show up to 3 definitions initially
  - Each definition will show an order number in bold, then the parts of speech. Below the actual definition will be shown
  - If there are more than 3 definitions available, a 'more' button will appear
    - If clicked it will expand the Definitions card to reveal rest of the definitions
    - Once expanded, the same can be contracted by clicking a less button- this will reduce the card size back to 3 definitions

- Below definition, another card will appear for Synonyms and Antonyms

  - Synonyms will appear on the left side of the card (if there is any)
  - Antonyms will appear on the right side of the card (if there is any)
  - If there is no synonym and antonym available, then the card will disappear
  - Similar to definitions card, each Synonym and Antonym will show available numbers next to header
  - Up to 3 synonyms and antonyms will appear initially
  - Similar to definitions card, both Synonyms and Antonyms can be expanded (if available)
  - All Synonyms and Antonyms are clickable, which will open a new word search window

- Last card is for Etymologies
  - Next to header, number of available etymology will appear
  - Up to 2 etymologies will be shown initially
  - All etymologies will be in ordered numbers
  - Similar to Definitions card, this card is also expandable if more that 2 are available

## Future plan (unimplemented)

- Store the word definitions locally once retrieved - so that it does not need to call backend every time
- Manually able to change app theme and color
