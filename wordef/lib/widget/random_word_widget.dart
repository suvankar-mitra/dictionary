import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:visibility_detector/visibility_detector.dart';

import '../model/app_constants.dart';
import '../../model/word_definition_model.dart';
import '../screen/definition_screen.dart';

class RandomWordDef extends StatefulWidget {
  const RandomWordDef({super.key});

  @override
  State<RandomWordDef> createState() => _RandomWordDefState();
}

class _RandomWordDefState extends State<RandomWordDef> {
  final String _randomWordApiUrl = "${AppConstants.apiUrl}/random";
  final int _maxTimerTick = 30;

  WordDefinition? _wordDefinition;
  late final Timer periodicTimer;
  late int _timerTick;
  bool isTimerTicking = true;

  @override
  void initState() {
    _timerTick = _maxTimerTick;
    isTimerTicking = true;
    periodicTimer = Timer.periodic(
      const Duration(seconds: 1),
      (timer) {
        // print("tick");
        setState(() {
          if (isTimerTicking) {
            _timerTick--;
            if (_timerTick <= 0) {
              _timerTick = _maxTimerTick;
            }
          }
        });
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    // _controller.dispose();
    periodicTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getRandomEnglishWord(),
        builder:
            (BuildContext context, AsyncSnapshot<WordDefinition?> snapshot) {
          if (snapshot.hasData) {
            WordDefinition? wd = snapshot.data;
            return InkWell(
              child: AnimatedSize(
                duration: const Duration(milliseconds: 250),
                child: Card(
                  elevation: 10,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Align(
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Transform(
                                  alignment: Alignment.center,
                                  transform: Matrix4.rotationY(math.pi),
                                  child: Icon(
                                    FontAwesomeIcons.leaf,
                                    size: Theme.of(context)
                                        .textTheme
                                        .labelMedium
                                        ?.fontSize,
                                    color: Theme.of(context)
                                        .textTheme
                                        .labelMedium
                                        ?.color,
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 6, vertical: 6),
                                  child: RichText(
                                    text: TextSpan(
                                        text: "Random Word Card",
                                        style: GoogleFonts.notoSansMono(
                                            color: Theme.of(context)
                                                .textTheme
                                                .labelSmall
                                                ?.color,
                                            fontSize: Theme.of(context)
                                                .textTheme
                                                .labelSmall
                                                ?.fontSize)),
                                  ),
                                ),
                                Icon(
                                  FontAwesomeIcons.leaf,
                                  size: Theme.of(context)
                                      .textTheme
                                      .labelMedium
                                      ?.fontSize,
                                  color: Theme.of(context)
                                      .textTheme
                                      .labelMedium
                                      ?.color,
                                ),
                              ],
                            )),
                        VisibilityDetector(
                          key: Key((RandomWordDef).toString()),
                          onVisibilityChanged: (info) => {
                            // widget is visible
                            isTimerTicking = info.visibleFraction > 0
                          },
                          child: GestureDetector(
                            onTap: () {
                              searchWordPageRoute(wd.word);
                            },
                            child: RichText(
                              text: TextSpan(
                                  text: wd!.word.trim(),
                                  style: GoogleFonts.notoSerif(
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .titleLarge
                                        ?.fontSize,
                                    fontWeight: FontWeight.bold,
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                  )),
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (int i = 0;
                                i < math.min(2, wd.meanings.length);
                                i++)
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    text: TextSpan(
                                        text:
                                            "(${wd.meanings.elementAt(i).partsOfSpeech.trim()})",
                                        style: GoogleFonts.notoSerif(
                                            fontSize: Theme.of(context)
                                                .textTheme
                                                .bodySmall
                                                ?.fontSize,
                                            color: Theme.of(context)
                                                .colorScheme
                                                .secondary)),
                                  ),
                                  RichText(
                                    text: TextSpan(
                                        text: sanitize(wd.meanings
                                            .elementAt(i)
                                            .meaning
                                            .trim()),
                                        style: GoogleFonts.notoSerif(
                                            fontSize: Theme.of(context)
                                                .textTheme
                                                .bodyMedium
                                                ?.fontSize,
                                            color: Theme.of(context)
                                                .textTheme
                                                .bodyMedium
                                                ?.color)),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  )
                                ],
                              ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                RichText(
                                  text: TextSpan(
                                      text: _timerTick < 10
                                          ? "0$_timerTick"
                                          : _timerTick.toString(),
                                      style: GoogleFonts.notoSansMono(
                                          color: Theme.of(context)
                                              .textTheme
                                              .labelSmall
                                              ?.color)),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 14),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          isTimerTicking = !isTimerTicking;
                                        });
                                      },
                                      icon: isTimerTicking
                                          ? const Icon(Icons.pause)
                                          : const Icon(Icons.play_arrow)),
                                ),
                                IconButton(
                                    onPressed: () {
                                      setState(() {
                                        isTimerTicking = true;
                                        _timerTick = _maxTimerTick;
                                      });
                                    },
                                    icon: const Icon(Icons.skip_next))
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              onTap: () {
                searchWordPageRoute(wd.word);
              },
            );
          } else {
            // print('string not generated yet');
            return const Text('Waiting for word...');
          }
        });
  }

  Future<WordDefinition?> getRandomEnglishWord() async {
    if (isTimerTicking && (_timerTick == _maxTimerTick)) {
      // print("getRandomEnglishWord $_timerTick $_maxTimerTick");
      isTimerTicking = false;
      var response = await http.get(Uri.parse(_randomWordApiUrl));

      if (response.statusCode == 200) {
        WordDefinition wordDefinition = wordDefinitionFromJson(response.body);
        // await player.setSource(UrlSource(wordDefinition.pronunciations.first));
        // String word = wordDefinition.word;
        // print('My word: ' + sanitize(wordDefinition.meanings.first.toString()));
        _wordDefinition = wordDefinition;
        isTimerTicking = true;
      } else {
        throw Exception('Unable to get Random Word');
      }
    }
    return _wordDefinition;
  }

  String sanitize(String s) => s.replaceAllMapped(
        RegExp(r'\\u([0-9a-fA-F]{4})'),
        (Match m) => String.fromCharCode(int.parse(m.group(1)!, radix: 16)),
      );

  void searchWordPageRoute(String word) {
    Navigator.push(
      context,
      CupertinoPageRoute(builder: (context) => DefinitionScreen(word: word)),
    );
  }
}
