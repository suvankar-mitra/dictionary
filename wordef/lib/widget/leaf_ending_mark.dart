import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LeafEndingMark extends StatelessWidget {
  const LeafEndingMark({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 10; i++)
          Icon(
            FontAwesomeIcons.pagelines,
            size: Theme.of(context).textTheme.labelLarge?.fontSize,
            color: Theme.of(context).textTheme.labelLarge?.color,
          ),
      ],
    );
  }
}
