import 'dart:math' as math;

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wordef/widget/leaf_ending_mark.dart';

import '../model/app_constants.dart';
import '../model/word_definition_model.dart';

class DefinitionScreen extends StatefulWidget {
  const DefinitionScreen(
      {super.key, required this.word, this.exactSearch = false});

  final String word;
  final bool exactSearch;

  @override
  State<DefinitionScreen> createState() => _DefinitionScreenState();
}

class _DefinitionScreenState extends State<DefinitionScreen> {
  final String _findWordApiUrl = "${AppConstants.apiUrl}/words/";
  final String _findWordExactApiUrl = "${AppConstants.apiUrl}/wordsexact/";

  final player = AudioPlayer();
  bool _gotWordDef = false;
  int _wordSearchReturnStatus = 0;
  WordDefinition? _wordDefinition;

  int _minDefCount = 1;
  int _maxDefCount = 1;
  bool _showMoreDef = true;
  List<WordMeaning> meanings = List.empty();

  int _minSynCount = 1;
  int _maxSynCount = 1;
  bool _showMoreSyn = true;
  List<String> synonyms = List.empty();

  int _minAntoCount = 1;
  int _maxAntoCount = 1;
  bool _showMoreAnto = true;
  List<String> antonyms = List.empty();

  int _minEtyCount = 1;
  int _maxEtyCount = 1;
  bool _showMoreEty = true;
  List<WordEtymology> etymologies = List.empty();

  List<String> otherDefinitions = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    player.setReleaseMode(ReleaseMode.stop);

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(Icons.arrow_back_ios_new),
                      // label: const Text(""),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.share),
                      // label: const Text(""),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                // padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FutureBuilder(
                        future: _getEnglishWordDefinition(widget.word),
                        builder: (BuildContext context,
                            AsyncSnapshot<WordDefinition?> snapshot) {
                          if (snapshot.hasData) {
                            WordDefinition? wd = snapshot.data;
                            // print("Building word $wd");
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    _buildTitle(wd, context),
                                    _buildPhonetics(wd, context),
                                    if (otherDefinitions.isNotEmpty)
                                      _buildOtherDefinitions(context),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                if (wd!.meanings.isNotEmpty)
                                  _buildDefinitions(context, wd),
                                const SizedBox(
                                  height: 10,
                                ),
                                if (synonyms.isNotEmpty || antonyms.isNotEmpty)
                                  _buildSynonymsAntonyms(wd, context),
                                if (synonyms.isNotEmpty || antonyms.isNotEmpty)
                                  const SizedBox(
                                    height: 10,
                                  ),
                                if (etymologies.isNotEmpty)
                                  _buildEtymologies(context, wd),
                                const SizedBox(
                                  height: 20,
                                ),
                                const LeafEndingMark(),
                              ],
                            );
                          } else {
                            if (404 == _wordSearchReturnStatus) {
                              return Center(
                                child: _buildNotFoundPage(context),
                              );
                            } else {
                              return Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Center(
                                  child: LoadingAnimationWidget
                                      .horizontalRotatingDots(
                                    size: Theme.of(context)
                                        .textTheme
                                        .displayLarge!
                                        .fontSize!,
                                    color:
                                        Theme.of(context).colorScheme.secondary,
                                  ),
                                ),
                              );
                            }
                          }
                        })
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container _buildNotFoundPage(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          RichText(
            text: TextSpan(
                text: "Oops! Requested word \"${widget.word}\" is not found. ",
                style: GoogleFonts.notoSerif(
                    fontSize: Theme.of(context).textTheme.labelLarge?.fontSize,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).textTheme.labelSmall?.color)),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                _googleSeachLaunch();
              },
              child: const Text("Search in Google"))
        ],
      ),
    );
  }

  Text _buildTitle(WordDefinition? wd, BuildContext context) {
    return Text.rich(
      TextSpan(
          text: wd?.word.trim(),
          style: GoogleFonts.notoSerif(
              fontSize: Theme.of(context).textTheme.displaySmall?.fontSize,
              color: Theme.of(context).colorScheme.primary)),
      textAlign: TextAlign.center,
    );
  }

  Container _buildOtherDefinitions(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RichText(
              text: TextSpan(
            style: GoogleFonts.notoSerif(
                fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                color: Theme.of(context).disabledColor),
            text: "Alternate definitions:",
          )),
          RichText(
              text: TextSpan(
            children: [
              for (String def in otherDefinitions)
                TextSpan(
                  text: def,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _searchWordExactPageRoute(def);
                    },
                )
            ],
            style: GoogleFonts.notoSerif(
                fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                color: Theme.of(context).disabledColor),
          )),
        ],
      ),
    );
  }

  Container _buildPhonetics(WordDefinition? wd, BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 4),
      child: RichText(
        text: TextSpan(
            children: [
              for (int i = 0; i < math.min(3, wd!.phonetics.length); i++)
                TextSpan(
                  text: "${sanitize(wd.phonetics.elementAt(i))}  ",
                ),
              if (wd.pronunciations.isNotEmpty)
                WidgetSpan(
                  child: GestureDetector(
                      onTap: () {
                        _audioPlay();
                      },
                      child: Icon(
                        FontAwesomeIcons.volumeHigh,
                        size: Theme.of(context).textTheme.bodyMedium?.fontSize,
                      )),
                )
            ],
            style: GoogleFonts.notoSerif(
              color: Theme.of(context).textTheme.labelMedium?.color,
              fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
            )),
      ),
    );
  }

  AnimatedSize _buildDefinitions(BuildContext context, WordDefinition wd) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 1000),
      curve: Curves.fastLinearToSlowEaseIn,
      alignment: Alignment.topCenter,
      child: Card(
        elevation: 4,
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: "DEFINITIONS ",
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              _searchWordPageRoute('definitions');
                            },
                        ),
                        TextSpan(
                          text: "  $_maxDefCount",
                          style: GoogleFonts.notoSerif(
                              fontWeight: FontWeight.normal,
                              color: Theme.of(context).disabledColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              _searchWordPageRoute('$_maxDefCount');
                            },
                        )
                      ],
                      style: GoogleFonts.notoSerif(
                          fontSize:
                              Theme.of(context).textTheme.labelLarge?.fontSize,
                          fontWeight: FontWeight.bold,
                          color:
                              Theme.of(context).textTheme.labelLarge?.color)),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              for (int i = 0; i < meanings.length; i++)
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: RichText(
                        text: TextSpan(
                            children: [
                              TextSpan(
                                text: "${i + 1}. ",
                                style: GoogleFonts.notoSerif(
                                    fontWeight: FontWeight.bold,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .labelLarge
                                        ?.fontSize,
                                    color: Theme.of(context)
                                        .textTheme
                                        .labelLarge
                                        ?.color),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    _searchWordPageRoute('${i + 1}');
                                  },
                              ),
                              TextSpan(
                                text: meanings.elementAt(i).partsOfSpeech,
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    var snackBar = SnackBar(
                                      content: Text(
                                          meanings.elementAt(i).source.trim()),
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  },
                              ),
                            ],
                            style: GoogleFonts.notoSerif(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .labelLarge
                                    ?.fontSize,
                                color:
                                    Theme.of(context).colorScheme.secondary)),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: SelectableText.rich(
                        TextSpan(
                            children: [
                              for (String word
                                  in meanings.elementAt(i).meaning.split(' '))
                                TextSpan(
                                  text: "${sanitize(word)} ",
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      _searchWordPageRoute(word);
                                    },
                                )
                            ],
                            style: GoogleFonts.notoSerif(
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.fontSize,
                                color: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.color)),
                      ),
                    ),
                    if (i < meanings.length - 1)
                      const SizedBox(
                        height: 6,
                      ),
                  ],
                ),
              if (_maxDefCount > _minDefCount)
                Align(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    onPressed: () {
                      setState(() {
                        if (_showMoreDef) {
                          meanings = wd.meanings.sublist(0, _maxDefCount);
                        } else {
                          meanings = wd.meanings.sublist(0, _minDefCount);
                        }
                        _showMoreDef = !_showMoreDef;
                      });
                    },
                    icon: Icon(
                      _showMoreDef ? Icons.more_horiz : Icons.expand_less_sharp,
                      size: Theme.of(context).textTheme.bodyLarge?.fontSize,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedSize _buildEtymologies(BuildContext context, WordDefinition wd) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 1000),
      curve: Curves.fastLinearToSlowEaseIn,
      alignment: Alignment.topCenter,
      child: Card(
        elevation: 4,
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: "ETYMOLOGY",
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              _searchWordPageRoute('ETYMOLOGY');
                            },
                        ),
                        TextSpan(
                            text: "  $_maxEtyCount",
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                _searchWordPageRoute('$_maxEtyCount');
                              },
                            style: GoogleFonts.notoSerif(
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context).disabledColor))
                      ],
                      style: GoogleFonts.notoSerif(
                          fontSize:
                              Theme.of(context).textTheme.labelLarge?.fontSize,
                          fontWeight: FontWeight.bold,
                          color:
                              Theme.of(context).textTheme.labelLarge?.color)),
                ),
              ),
              const SizedBox(
                height: 6,
              ),
              for (int i = 0; i < etymologies.length; i++)
                Container(
                  margin: const EdgeInsets.only(top: 4),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: SelectableText.rich(
                      TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: "${i + 1}. ",
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    _searchWordPageRoute("${i + 1}");
                                  },
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold)),
                            TextSpan(children: [
                              for (String e in (etymologies
                                          .elementAt(i)
                                          .etymology
                                          .startsWith("[")
                                      ? etymologies
                                          .elementAt(i)
                                          .etymology
                                          .substring(
                                              1,
                                              etymologies
                                                      .elementAt(i)
                                                      .etymology
                                                      .length -
                                                  1)
                                      : etymologies.elementAt(i).etymology)
                                  .split(" "))
                                TextSpan(
                                  text: sanitize("$e "),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      _searchWordPageRoute(sanitize(e));
                                    },
                                )
                            ]),
                          ],
                          style: GoogleFonts.notoSerif(
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.fontSize,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.color)),
                    ),
                  ),
                ),
              if (_maxEtyCount > _minEtyCount)
                Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    icon: Icon(
                      _showMoreEty ? Icons.more_horiz : Icons.expand_less_sharp,
                      size: Theme.of(context).textTheme.bodyLarge?.fontSize,
                    ),
                    onPressed: () {
                      setState(() {
                        if (_showMoreEty) {
                          etymologies = wd.etymologies.sublist(0, _maxEtyCount);
                        } else {
                          etymologies = wd.etymologies.sublist(0, _minEtyCount);
                        }
                        _showMoreEty = !_showMoreEty;
                      });
                    },
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  AnimatedSize _buildSynonymsAntonyms(WordDefinition wd, BuildContext context) {
    return AnimatedSize(
      duration: const Duration(milliseconds: 1000),
      curve: Curves.fastLinearToSlowEaseIn,
      alignment: Alignment.topCenter,
      child: Card(
        elevation: 4,
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (wd.synonyms.isNotEmpty) _buildSynonyms(context, wd),
              if (wd.antonyms.isNotEmpty) _buildAntonyms(context, wd),
            ],
          ),
        ),
      ),
    );
  }

  Column _buildAntonyms(BuildContext context, WordDefinition wd) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "ANTONYMS",
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _searchWordPageRoute('ANTONYMS');
                    },
                ),
                TextSpan(
                    text: "  $_maxAntoCount",
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        _searchWordPageRoute('$_maxAntoCount');
                      },
                    style: GoogleFonts.notoSerif(
                        fontWeight: FontWeight.normal,
                        color: Theme.of(context).disabledColor))
              ],
              style: GoogleFonts.notoSerif(
                  fontSize: Theme.of(context).textTheme.labelLarge?.fontSize,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).textTheme.labelLarge?.color)),
        ),
        if (antonyms.isNotEmpty)
          const SizedBox(
            height: 10,
          ),
        for (String anto in antonyms)
          RichText(
            text: TextSpan(
                children: [
                  for (String a in anto.split(' '))
                    TextSpan(
                      text: "$a ",
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          _searchWordPageRoute(sanitize(a));
                        },
                    )
                ],
                style: GoogleFonts.notoSerif(
                    fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                    color: Theme.of(context).textTheme.bodyMedium?.color)),
          ),
        if (_maxAntoCount > _minAntoCount)
          Align(
            alignment: Alignment.centerLeft,
            child: IconButton(
              icon: Icon(
                _showMoreAnto ? Icons.more_horiz : Icons.expand_less_sharp,
                size: Theme.of(context).textTheme.bodyLarge?.fontSize,
              ),
              onPressed: () {
                setState(() {
                  if (_showMoreAnto) {
                    antonyms = wd.antonyms.sublist(0, _maxAntoCount);
                  } else {
                    antonyms = wd.antonyms.sublist(0, _minAntoCount);
                  }
                  _showMoreAnto = !_showMoreAnto;
                });
              },
            ),
          ),
      ],
    );
  }

  Column _buildSynonyms(BuildContext context, WordDefinition wd) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "SYNONYMS",
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _searchWordPageRoute('SYNONYMS');
                    },
                ),
                TextSpan(
                    text: "  $_maxSynCount",
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        _searchWordPageRoute('$_maxSynCount');
                      },
                    style: GoogleFonts.notoSerif(
                        fontWeight: FontWeight.normal,
                        color: Theme.of(context).disabledColor))
              ],
              style: GoogleFonts.notoSerif(
                  fontSize: Theme.of(context).textTheme.labelLarge?.fontSize,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).textTheme.labelLarge?.color)),
        ),
        if (synonyms.isNotEmpty)
          const SizedBox(
            height: 10,
          ),
        for (String syn in synonyms)
          RichText(
            text: TextSpan(
                children: [
                  for (String s in syn.split(" "))
                    TextSpan(
                      text: "$s ",
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          _searchWordPageRoute(sanitize(s));
                        },
                    )
                ],
                style: GoogleFonts.notoSerif(
                    fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                    color: Theme.of(context).textTheme.bodyMedium?.color)),
          ),
        if (_maxSynCount > _minSynCount)
          Align(
            alignment: Alignment.bottomCenter,
            child: IconButton(
              icon: Icon(
                _showMoreSyn ? Icons.more_horiz : Icons.expand_less_sharp,
                size: Theme.of(context).textTheme.bodyLarge?.fontSize,
              ),
              onPressed: () {
                setState(() {
                  if (_showMoreSyn) {
                    synonyms = wd.synonyms.sublist(0, _maxSynCount);
                  } else {
                    synonyms = wd.synonyms.sublist(0, _minSynCount);
                  }
                  _showMoreSyn = !_showMoreSyn;
                });
              },
            ),
          ),
      ],
    );
  }

  Future<WordDefinition?> _getEnglishWordDefinition(String word) async {
    // print("getEnglishWordDefinition $word ");

    if (!_gotWordDef) {
      // print(Uri.parse(
      // (widget.exactSearch ? _findWordExactApiUrl : _findWordApiUrl) +
      // word));
      var response = await http.get(Uri.parse(
          (widget.exactSearch ? _findWordExactApiUrl : _findWordApiUrl) +
              word));

      // print(
      // "getEnglishWordDefinition returned from REST ${response.statusCode}");

      if (response.statusCode == 200) {
        // print("getEnglishWordDefinition fromJsonList : ${response.statusCode}");
        List<WordDefinition> wordDefinitionList =
            WordDefinition.wordDefinitionListFromJson(response.body);
        // print(wordDefinitionList);

        WordDefinition wordDefinition = wordDefinitionList.first;

        // if there are more than one definition
        for (int i = 1; i < wordDefinitionList.length; i++) {
          otherDefinitions.add(wordDefinitionList.elementAt(i).word);
        }

        if (wordDefinition.pronunciations.isNotEmpty) {
          await player
              .setSource(UrlSource(wordDefinition.pronunciations.first));
        }
        // String word = wordDefinition.word;
        // print('found word: ' + sanitize(wordDefinition.word));
        _wordDefinition = wordDefinition;

        _minDefCount = math.min(wordDefinition.meanings.length, 3);
        _maxDefCount = math.max(wordDefinition.meanings.length, _minDefCount);
        meanings = wordDefinition.meanings.sublist(0, _minDefCount);

        _minSynCount = math.min(wordDefinition.synonyms.length, 3);
        _maxSynCount = math.max(wordDefinition.synonyms.length, _minSynCount);
        synonyms = wordDefinition.synonyms.sublist(0, _minSynCount);

        _minAntoCount = math.min(wordDefinition.antonyms.length, 3);
        _maxAntoCount = math.max(wordDefinition.antonyms.length, _minAntoCount);
        antonyms = wordDefinition.antonyms.sublist(0, _minAntoCount);

        _minEtyCount = math.min(wordDefinition.etymologies.length, 2);
        _maxEtyCount =
            math.max(wordDefinition.etymologies.length, _minEtyCount);
        etymologies = wordDefinition.etymologies.sublist(0, _minEtyCount);

        _gotWordDef = true;
        _wordSearchReturnStatus = 200;

        _persistSearchWord(word);
      } else if (response.statusCode == 404) {
        _wordSearchReturnStatus = 404;
        // print("404 not found");
      } else {
        throw Exception('Unable to get Word definition');
      }
    }
    // print("getEnglishWordDefinition returing ${_wordDefinition?.word}");
    return _wordDefinition;
  }

  Future<void> _audioPlay() async {
    // print("Starting audio play ${player.source}");
    await player.resume();
    //setState(() => _playerState = PlayerState.playing);
  }

  String sanitize(String s) => s
      .replaceAllMapped(
        RegExp(r'\\u([0-9a-fA-F]{4})'),
        (Match m) => String.fromCharCode(int.parse(m.group(1)!, radix: 16)),
      )
      .replaceAll("\\\"", "\"");

  void _searchWordPageRoute(final String word) async {
    String removePunctWord = word.replaceAll(RegExp(r'[^\w\s]+'), '');
    Navigator.push(
      context,
      CupertinoPageRoute(
          builder: (context) => DefinitionScreen(word: removePunctWord)),
    );
  }

  void _searchWordExactPageRoute(final String word) async {
    String removePunctWord = word.replaceAll(RegExp(r'[^\w\s]+'), '');
    Navigator.push(
      context,
      CupertinoPageRoute(
          builder: (context) => DefinitionScreen(
                word: removePunctWord,
                exactSearch: true,
              )),
    );
  }

  void _googleSeachLaunch() async {
    const String googleUrl = "https://www.google.com/search?q=";
    String searchUrl = Uri.encodeFull(googleUrl + widget.word);
    if (!await launchUrl(Uri.parse(searchUrl))) {
      throw Exception('Could not launch $searchUrl');
    }
  }

  void _persistSearchWord(String word) async {
    final prefs = await SharedPreferences.getInstance();

    List<String> searchWordList =
        prefs.getStringList('search_words') ?? <String>[];

    if (searchWordList.isNotEmpty) {
      searchWordList.insert(0, word);
    } else {
      searchWordList.add(word);
    }

    if (searchWordList.length > 100) {
      searchWordList = searchWordList.sublist(0, 19);
    }

    await prefs.setStringList('search_words', searchWordList);
  }
}
