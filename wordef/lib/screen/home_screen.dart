import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:url_launcher/url_launcher.dart';

import '../widget/random_word_widget.dart';
import 'definition_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  late FocusNode focus;
  final TextEditingController _controller = TextEditingController();

  // final int _minRecentListSize = 20;

  @override
  void initState() {
    super.initState();
    focus = FocusNode();
    _internetConnectionCheck();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 5,
          toolbarHeight: 60, // Set this height
          centerTitle: true,
          automaticallyImplyLeading: false,
          flexibleSpace: Container(
            alignment: Alignment.bottomCenter,
            // padding: const EdgeInsets.all(20),
            child: RichText(
              text: TextSpan(
                  text: "Dictionary",
                  style: GoogleFonts.notoSerif(
                      fontSize:
                          Theme.of(context).textTheme.displaySmall?.fontSize,
                      color: Theme.of(context).textTheme.displaySmall?.color)),
            ),
          ),
        ),
        bottomNavigationBar: TextButton(
          onPressed: () {
            _gitlabLaunch();
          },
          child: RichText(
            text: TextSpan(
                children: [
                  const TextSpan(
                    text: "built with ",
                  ),
                  WidgetSpan(
                    child: Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: Theme.of(context).textTheme.bodyLarge?.fontSize,
                    ),
                  ),
                ],
                style: GoogleFonts.notoSans(
                    fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                    color: Theme.of(context).textTheme.labelLarge?.color)),
          ),
        ),
        body: SafeArea(
          child: RefreshIndicator(
            onRefresh: _pullRefresh,
            child: Container(
                margin: const EdgeInsets.only(left: 16, right: 16),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 10),
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 10),
                        child: SearchBar(
                          padding: const MaterialStatePropertyAll<EdgeInsets>(
                              EdgeInsets.symmetric(horizontal: 16.0)),
                          leading: const Icon(Icons.search),
                          shape: MaterialStatePropertyAll<OutlinedBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(14))),
                          trailing: <Widget>[
                            IconButton(
                                onPressed: focus.hasFocus
                                    ? () {
                                        _controller.clear();
                                      }
                                    : null,
                                icon: focus.hasFocus
                                    ? const Icon(Icons.close)
                                    : MediaQuery.of(context)
                                                .platformBrightness ==
                                            Brightness.dark
                                        ? const Icon(Icons.dark_mode)
                                        : const Icon(Icons.sunny)),
                          ],
                          controller: _controller,
                          focusNode: focus,
                          hintText: "Search a word",
                          textInputAction: TextInputAction.search,
                          onSubmitted: (value) {
                            setState(() {
                              focus.unfocus();
                              _controller.clear();
                            });
                            _searchWordPageRoute(value.trim());
                          },
                          onTap: () {
                            setState(() {
                              focus.requestFocus();
                            });
                          },
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: const RandomWordDef(),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      /* Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            VisibilityDetector(
                              key: Key((HomeScreen).toString()),
                              onVisibilityChanged: (VisibilityInfo info) {
                                setState(() {
                                  _buildSearchWordList();
                                });
                              },
                              child: RichText(
                                text: TextSpan(
                                    text: "RECENT",
                                    style: GoogleFonts.notoSerif(
                                        fontSize: Theme.of(context)
                                            .textTheme
                                            .labelLarge
                                            ?.fontSize,
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context)
                                            .textTheme
                                            .labelLarge
                                            ?.color)),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            FutureBuilder(
                                future: _buildSearchWordList(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    List<String> recentSearches =
                                        snapshot.data!.toList();
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              for (int i = 0;
                                                  i <
                                                      (recentSearches.length /
                                                              2)
                                                          .ceil();
                                                  i++)
                                                _getRecentSearchElement(
                                                    recentSearches, i, context),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              for (int i =
                                                      (recentSearches.length /
                                                              2)
                                                          .ceil();
                                                  i < recentSearches.length;
                                                  i++)
                                                _getRecentSearchElement(
                                                    recentSearches, i, context),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  } else {
                                    return const Text("Nothing searched yet");
                                  }
                                })
                          ],
                        ),
                      ), */
                    ],
                  ),
                )),
          ),
        ));
  }

  /* Container _getRecentSearchElement(
      List<String> recentSearches, int i, BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 6),
      child: GestureDetector(
        onTap: () {
          _searchWordPageRoute(recentSearches.elementAt(i));
        },
        child: RichText(
          text: TextSpan(
              text: recentSearches.elementAt(i),
              style: GoogleFonts.notoSerif(
                  decoration: TextDecoration.underline,
                  decorationStyle: TextDecorationStyle.dotted,
                  fontSize: Theme.of(context).textTheme.bodyMedium?.fontSize,
                  color: Theme.of(context).textTheme.bodyMedium?.color)),
        ),
      ),
    );
  } */

  void _searchWordPageRoute(String word) async {
    if (word.isNotEmpty) {
      Navigator.push(
        context,
        CupertinoPageRoute(builder: (context) => DefinitionScreen(word: word)),
      );
    }
  }

  /* Future<List<String>> _buildSearchWordList() async {
    final prefs = await SharedPreferences.getInstance();
    List<String> swList = prefs.getStringList('search_words') ?? <String>[];
    swList = swList.toSet().toList();
    swList = swList.sublist(0, min(_minRecentListSize, swList.length));
    return swList;
  } */

  _gitlabLaunch() async {
    const String gitlabUrl = "https://gitlab.com/suvankar-mitra/dictionary";
    if (!await launchUrl(Uri.parse(gitlabUrl))) {
      throw Exception('Could not launch $gitlabUrl');
    }
  }

  _internetConnectionCheck() async {
    bool result = await InternetConnectionChecker().hasConnection;
    if (result) {
      // print('_internetConnectionCheck YAY! Free cute dog pics!');
      Fluttertoast.showToast(
          msg: "Connected to internet",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
    } else {
      // print('_internetConnectionCheck No internet :( Reason');
      Fluttertoast.showToast(
          msg: "No internet!",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
    }
  }

  Future<void> _pullRefresh() async {}
}
