import 'package:flutter/material.dart';

class AppColors {
  static const darkBlue = Color(0xFF1E1E2C);
  static const lightBlue = Color(0xFF2D2D44);
  static const darkRed = Color(0xFF4E0423);
  static const darkGrey = Color(0xFF121212);
  static const lightThemeSearchBarShadowColor = Color(0xff542e2e);
  static const lightThemeSearchBarSurfaceTintColor = Colors.white70;
}
