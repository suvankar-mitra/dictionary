import 'package:flutter/material.dart';

class AppThemes {
  static ColorScheme colorSchemeDark = ColorScheme.fromSeed(
    brightness: Brightness.dark,
    seedColor: Colors.lime,
  );

  static ColorScheme colorSchemeLight = ColorScheme.fromSeed(
    brightness: Brightness.light,
    seedColor: Colors.lime,
  );

  static final lightTheme = ThemeData(
    colorScheme: colorSchemeLight,
  );
  static final darkTheme = ThemeData(colorScheme: colorSchemeDark);
}
