import 'dart:convert';

WordDefinition wordDefinitionFromJson(String str) =>
    WordDefinition.fromJson(json.decode(str));

// List<WordDefinition> wordDefinitionListFromJson(String str) =>
//     WordDefinition.fromJsonList(
//         (json.decode(str)) as List<Map<String, dynamic>>);

class WordDefinition {
  String word;
  List<WordMeaning> meanings;
  List<WordEtymology> etymologies;
  List<String> synonyms;
  List<String> antonyms;
  List<String> phonetics;
  List<String> pronunciations;

  WordDefinition(
      {required this.word,
      required this.meanings,
      required this.etymologies,
      required this.synonyms,
      required this.antonyms,
      required this.phonetics,
      required this.pronunciations});

  factory WordDefinition.fromJson(Map<String, dynamic> json) => WordDefinition(
        word: json["word"],
        meanings: List<WordMeaning>.from(
            json["meanings"].map((e) => WordMeaning.fromJson(e))),
        etymologies: List<WordEtymology>.from(
            json["etymologies"].map((e) => WordEtymology.fromJson(e))),
        synonyms: List<String>.from(json["synonyms"]),
        antonyms: List<String>.from(json["antonyms"]),
        phonetics: List<String>.from(json["phonetics"]),
        pronunciations: List<String>.from(json["pronunciations"]),
      );

  static List<WordDefinition> wordDefinitionListFromJson(String str) {
    var jsons = json.decode(str);
    // print("fromJsonList: $jsons");
    List<WordDefinition> wdList = [];
    for (Map<String, dynamic> json in jsons) {
      wdList.add(WordDefinition.fromJson(json));
    }
    return wdList;
  }
}

class WordEtymology {
  String etymology;
  String source;

  WordEtymology({required this.etymology, required this.source});

  factory WordEtymology.fromJson(Map<String, dynamic> json) =>
      WordEtymology(etymology: json["etymology"], source: json["source"]);
}

class WordMeaning {
  WordMeaning(
      {required this.meaning,
      required this.partsOfSpeech,
      required this.source});
  String meaning;
  String partsOfSpeech;
  String source;

  factory WordMeaning.fromJson(Map<String, dynamic> json) => WordMeaning(
      meaning: json["meaning"],
      partsOfSpeech: json["partsOfSpeech"],
      source: json["source"]);
}
