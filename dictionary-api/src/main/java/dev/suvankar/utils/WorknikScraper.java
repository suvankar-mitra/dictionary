package dev.suvankar.utils;

import dev.suvankar.model.WordDefinition;
import dev.suvankar.model.WordEtymology;
import dev.suvankar.model.WordMeaning;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

public class WorknikScraper {
    private final static String SOURCE_URL_WORDNIK = "https://www.wordnik.com/words/";

    public static WordDefinition getWordDefinition(String word) {

        WordDefinition wd = new WordDefinition();
        wd.setWord(word);

        Document doc;
        try {
            String url = SOURCE_URL_WORDNIK + word;
            doc = Jsoup.connect(url)
                    .userAgent(
                            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36")
                    .get();

            // definitions
            Element definitionDiv = doc.select("div.word-module.module-definitions > div.guts.active").first();

            if (definitionDiv != null) {
                String source = "";
                for (Element element : definitionDiv.children()) {
                    if (element.tag().getName().equals("h3")) {
                        if (element.hasClass("source")) {
                            source = element.ownText().trim();
                        }
                    } else if (element.tag().getName().equals("ul")) {
                        for (Element li : element.children()) {
                            String pos = "";
                            for (Element liEl : li.children()) {
                                if (liEl.tagName().equals("abbr")) {
                                    if (liEl.attr("title").equals("partOfSpeech")) {
                                        pos = liEl.ownText().trim();
                                        break;
                                    }
                                }

                            }
                            if (!pos.isEmpty()) {
                                WordMeaning wm = new WordMeaning();
                                wm.setSource(source);
                                wm.setMeaning(li.text().replace(pos, "").trim());
                                wm.setPartsOfSpeech(pos);
                                wd.getMeanings().add(wm);
                            }
                        }
                    }
                }
            }

            // etymologies
            Element etyDiv = doc.select("div.word-module.module-etymologies > div.guts").first();
            if (etyDiv != null) {
                String source = "";
                for (Element element : etyDiv.children()) {
                    if (element.tag().getName().equals("h3")) {
                        if (element.hasClass("source")) {
                            source = element.ownText();
                        }
                    } else {
                        Element ety = element.selectFirst("div.sub-module");
                        if (ety != null) {
                            WordEtymology etymology = new WordEtymology();
                            etymology.setEtymology(ety.text().trim());
                            etymology.setSource(source);
                            wd.getEtymologies().add(etymology);
                        }
                    }
                }
            }

            // synonyms
            Element synDiv = doc.select("div.synonym").first();
            if (synDiv != null) {
                Element synOl = synDiv.selectFirst("ol");
                if (synOl != null) {
                    for (Element element : synOl.children()) {
                        wd.getSynonyms().add(element.text().trim());
                    }
                }
            }

            // synonyms
            Element antDiv = doc.select("div.antonym").first();
            if (antDiv != null) {
                Element antOl = antDiv.selectFirst("ol");
                if (antOl != null) {
                    for (Element element : antOl.children()) {
                        wd.getAntonyms().add(element.text().trim());
                    }
                }
            }

            // phonetics
            wd.getPhonetics().addAll(WikitionaryScaper.getWordIPA(word));
            // System.out.println(wd.getPhonetics());

            // pronunciation audio urls
            wd.getPronunciations().addAll(WikitionaryScaper.getPronunciationAudio(word));

        } catch (IOException e) {
            System.err.println(e);
        }

        return wd;
    }
}
