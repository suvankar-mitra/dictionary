package dev.suvankar.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WikitionaryScaper {
    private final static String SOURCE_URL_WIKITIONARY = "https://en.wiktionary.org/wiki/";

    public static List<String> getWordIPA(String word) {

        List<String> ipaList = new ArrayList<>();
        Document doc;

        try {
            String url = SOURCE_URL_WIKITIONARY + word;
            doc = Jsoup.connect(url)
                    .userAgent(
                            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36")
                    .get();

            Elements ipaElements = doc.select("span.IPA");
            for (Element el : ipaElements) {
                ipaList.add(el.text());
            }
        } catch (IOException e) {
            System.err.println(e);
        }
        return ipaList;
    }

    public static List<String> getPronunciationAudio(String word) {
        List<String> audioList = new ArrayList<>();
        Document doc;

        try {
            String url = SOURCE_URL_WIKITIONARY + word;
            doc = Jsoup.connect(url)
                    .userAgent(
                            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36")
                    .get();
            // try again with lower case
            if (doc == null) {
                url = SOURCE_URL_WIKITIONARY + word.toLowerCase();
                doc = Jsoup.connect(url)
                        .userAgent(
                                "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36")
                        .get();
            }
            if (doc != null) {

                Element audioTable = doc.select("table.audiotable").first();
                if (audioTable != null) {
                    Elements rows = audioTable.select("tr");
                    if (rows != null) {
                        // System.out.println(rows);
                        for (Element row : rows) {
                            Element audioTd = row.select("td.audiofile").first();
                            Element audioSrc = audioTd.select("audio").first();
                            Elements sources = audioSrc.select("source");
                            if (sources != null) {
                                for (Element element : sources) {
                                    String audioString = element.absUrl("src");
                                    // System.out.println(audioString);
                                    audioList.add(audioString);
                                }
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }
        return audioList;
    }
}
