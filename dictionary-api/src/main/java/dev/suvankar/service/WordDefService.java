package dev.suvankar.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

import dev.suvankar.model.WordDefinition;
import dev.suvankar.repository.WordDefRepository;
import dev.suvankar.utils.WorknikScraper;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class WordDefService {

    private WordDefRepository repository;

    public List<WordDefinition> getWordDefinition(String word) {

        if (repository.existsByWord(word)) {
            return repository.findByWord(word);
        }

        WordDefinition wd = WorknikScraper.getWordDefinition(word);

        if (wd.getMeanings().isEmpty()) {
            // try again with all lower case
            wd = WorknikScraper.getWordDefinition(word.toLowerCase());
            if (wd.getMeanings().isEmpty()) {
                // try again with Fist letter capitalized
                wd = WorknikScraper.getWordDefinition(word.substring(0, 1).toUpperCase()
                        + (word.length() > 1 ? word.substring(1).toLowerCase() : ""));
                if (wd.getMeanings().isEmpty())
                    return null;
            }
        }

        WordDefinition wdSave = new WordDefinition();
        wdSave.setAntonyms(wd.getAntonyms());
        wdSave.setEtymologies(wd.getEtymologies());
        wdSave.setMeanings(wd.getMeanings());
        wdSave.setSynonyms(wd.getSynonyms());
        wdSave.setWord(wd.getWord());
        wdSave.setPhonetics(wd.getPhonetics());
        wdSave.setPronunciations(wd.getPronunciations());

        repository.save(wd);
        return repository.findByWord(word);
    }

    public List<WordDefinition> getWordExactDefinition(String word) {

        if (repository.existsByWordExact(word)) {
            return repository.findByWordExact(word);
        }

        WordDefinition wd = WorknikScraper.getWordDefinition(word);

        if (wd.getMeanings().isEmpty())
            return null;

        WordDefinition wdSave = new WordDefinition();
        wdSave.setAntonyms(wd.getAntonyms());
        wdSave.setEtymologies(wd.getEtymologies());
        wdSave.setMeanings(wd.getMeanings());
        wdSave.setSynonyms(wd.getSynonyms());
        wdSave.setWord(wd.getWord());
        wdSave.setPhonetics(wd.getPhonetics());
        wdSave.setPronunciations(wd.getPronunciations());

        repository.save(wd);
        return repository.findByWord(word);
    }

    public Optional<WordDefinition> getRandomWord() {

        return Optional.ofNullable(repository.getRandomWord());
    }

}
