package dev.suvankar.repository;

import dev.suvankar.model.WordDefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface WordDefRepository extends JpaRepository<WordDefinition, Long> {
    @Query("SELECT w FROM WordDefinition w WHERE lower(:word)=lower(w.word)")
    List<WordDefinition> findByWord(@Param("word") String word);

    @Query("SELECT CASE WHEN COUNT(w) > 0 THEN true ELSE false END FROM WordDefinition w WHERE lower(:word)=lower(w.word)")
    boolean existsByWord(@Param("word") String word);

    @Query("SELECT w FROM WordDefinition w ORDER BY RAND() LIMIT 1 ")
    WordDefinition getRandomWord();

    @Query("SELECT CASE WHEN COUNT(w) > 0 THEN true ELSE false END FROM WordDefinition w WHERE :word=w.word")
    boolean existsByWordExact(String word);

    @Query("SELECT w FROM WordDefinition w WHERE :word=w.word")
    List<WordDefinition> findByWordExact(String word);
}
