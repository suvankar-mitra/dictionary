package dev.suvankar.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Entity
@Table
public class WordDefinition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String word;

    @OneToMany(targetEntity = WordMeaning.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "id", name = "word_definition_id")
    private List<WordMeaning> meanings;

    @OneToMany(targetEntity = WordEtymology.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(referencedColumnName = "id", name = "word_definition_id")
    private List<WordEtymology> etymologies;

    private Set<String> synonyms;
    private Set<String> antonyms;
    private Set<String> phonetics;
    private List<String> pronunciations;

    public WordDefinition() {
        meanings = new ArrayList<>(5);
        etymologies = new ArrayList<>(5);
        synonyms = new HashSet<>();
        antonyms = new HashSet<>();
        phonetics = new HashSet<>();
        pronunciations = new ArrayList<>();
    }
}
