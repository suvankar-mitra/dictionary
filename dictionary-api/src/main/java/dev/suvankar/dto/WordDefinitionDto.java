package dev.suvankar.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
public class WordDefinitionDto {
    private String word;
    private List<WordMeaningDto> meanings;
    private List<WordEtymologyDto> etymologies;
    private Set<String> synonyms;
    private Set<String> antonyms;
    private Set<String> phonetics;
    private List<String> pronunciations;

    public WordDefinitionDto() {
        meanings = new ArrayList<>(5);
        etymologies = new ArrayList<>(5);
        synonyms = new HashSet<>();
        antonyms = new HashSet<>();
        phonetics = new HashSet<>();
        pronunciations = new ArrayList<>();
    }
}
