package dev.suvankar.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class WordMeaningDto {
    private String meaning;
    private String partsOfSpeech;
    private String source;
}
