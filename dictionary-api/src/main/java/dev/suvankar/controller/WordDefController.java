package dev.suvankar.controller;

import dev.suvankar.dto.WordDefinitionDto;
import dev.suvankar.dto.WordEtymologyDto;
import dev.suvankar.dto.WordMeaningDto;
import dev.suvankar.model.WordDefinition;
import dev.suvankar.model.WordEtymology;
import dev.suvankar.model.WordMeaning;
import dev.suvankar.service.WordDefService;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class WordDefController {

    private WordDefService service;

    private static final Logger logger = LoggerFactory.getLogger(WordDefController.class);

    public record Message(String msg) {
    }

    @GetMapping("/about")
    public ResponseEntity<Message> getAbout() {
        return ResponseEntity.ok(new Message(
                "(v1) This is an English dictionary API. Endpoints are: /api/v1/about, /api/v1/random, /api/v1/words/<param>"));
    }

    @GetMapping("/random")
    public ResponseEntity<WordDefinitionDto> getRandomWordFromDatabase() {
        Optional<WordDefinition> wdo = service.getRandomWord();
        if (wdo.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        WordDefinition wd = wdo.get();
        WordDefinitionDto dto = wordDefModelToDtoMapper(wd);

        logger.info("Requested random word: " + dto.getWord());

        return ResponseEntity.ok(dto);
    }

    /*
     * @GetMapping("/audio/{word}")
     * public List<String> getWordAudio(@PathVariable String word) {
     * return WikitionaryScaper.getPronunciationAudio(word);
     * }
     * 
     * @GetMapping("/ipa/{word}")
     * public List<String> getIPAs(@PathVariable String word) {
     * // return WikitionaryScaper.getWordIPA(word);
     * List<String> uniipa = new ArrayList<>();
     * for (String i : WikitionaryScaper.getWordIPA(word)) {
     * uniipa.add(StringEscapeUtils.escapeJava(i));
     * }
     * return uniipa;
     * }
     */

    /*
     * private String unicodeToUnicodeCurly(String uniStr) {
     * StringBuilder stb = new StringBuilder();
     * char cache = '\0';
     * for (int i = 0; i < uniStr.length(); i++) {
     * stb.append(uniStr.charAt(i));
     * 
     * if (uniStr.charAt(i) == '\\') {
     * cache = uniStr.charAt(i);
     * continue;
     * }
     * if (cache == '\\') {
     * if (uniStr.charAt(i) == 'u') {
     * stb.append("{").append(uniStr.substring(i + 1, i + 5)).append("}");
     * i += 4;
     * cache = '\0';
     * continue;
     * }
     * }
     * }
     * return stb.toString();
     * }
     */

    @GetMapping("/words")
    public ResponseEntity<List<WordDefinitionDto>> getWordDefinition(@RequestParam("word") String word) {
        return getWordDefinitionByPath(word);
    }

    @GetMapping("/words/{word}")
    public ResponseEntity<List<WordDefinitionDto>> getWordDefinitionByPath(@PathVariable String word) {

        logger.info("Requested word: " + word);

        List<WordDefinition> wordDefinitionList = service.getWordDefinition(word);

        if (wordDefinitionList == null || wordDefinitionList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        List<WordDefinitionDto> wdDtoList = wordDefModelToDtoMapperList(wordDefinitionList);
        logger.info("Found word: " + wdDtoList.size());
        ;
        return ResponseEntity.ok(wdDtoList);

    }

    @GetMapping("/wordsexact/{word}")
    public ResponseEntity<List<WordDefinitionDto>> getWordExactDefinitionByPath(@PathVariable String word) {

        logger.info("Requested exact word: " + word);

        List<WordDefinition> wordDefinitionList = service.getWordExactDefinition(word);

        if (wordDefinitionList == null || wordDefinitionList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        List<WordDefinitionDto> wdDtoList = wordDefModelToDtoMapperList(wordDefinitionList);
        logger.info("Found word: " + wdDtoList.size());
        ;
        return ResponseEntity.ok(wdDtoList);

    }

    private WordDefinitionDto wordDefModelToDtoMapper(WordDefinition wd) {
        if (wd == null) {
            return null;
        }

        WordDefinitionDto dto = new WordDefinitionDto();

        dto.setWord(wd.getWord());
        dto.setMeanings(wordMeaningModelToDto(wd.getMeanings().stream().map((WordMeaning wm) -> {
            wm.setMeaning(StringEscapeUtils.escapeJava(wm.getMeaning()));
            return wm;
        }).toList()));
        dto.setEtymologies(wordEtyModelToDto(wd.getEtymologies().stream().map((we) -> {
            we.setEtymology(StringEscapeUtils.escapeJava(we.getEtymology()));
            return we;
        }).toList()));
        dto.getAntonyms().addAll(wd.getAntonyms());
        dto.getSynonyms().addAll(wd.getSynonyms());
        dto.getPhonetics().addAll(
                wd.getPhonetics().stream().map(pr -> StringEscapeUtils.escapeJava(pr)).toList());
        // dto.getPhonetics().addAll(wd.getPhonetics());
        dto.getPronunciations()
                .addAll(wd.getPronunciations());

        return dto;
    }

    private List<WordDefinitionDto> wordDefModelToDtoMapperList(List<WordDefinition> wdList) {
        if (wdList == null || wdList.isEmpty()) {
            return new ArrayList<>();
        }
        List<WordDefinitionDto> wdDtoList = new ArrayList<>(wdList.size());

        for (WordDefinition wd : wdList) {
            WordDefinitionDto dto = new WordDefinitionDto();

            dto.setWord(wd.getWord());
            dto.setMeanings(wordMeaningModelToDto(wd.getMeanings().stream().map((wm) -> {
                wm.setMeaning(StringEscapeUtils.escapeJava(wm.getMeaning()));
                return wm;
            }).toList()));
            dto.setEtymologies(wordEtyModelToDto(wd.getEtymologies().stream().map((we) -> {
                we.setEtymology(StringEscapeUtils.escapeJava(we.getEtymology()));
                return we;
            }).toList()));
            dto.getAntonyms().addAll(wd.getAntonyms());
            dto.getSynonyms().addAll(wd.getSynonyms());
            dto.getPhonetics().addAll(wd.getPhonetics().stream()
                    .map(pr -> StringEscapeUtils.escapeJava(pr)).toList());
            dto.getPronunciations()
                    .addAll(wd.getPronunciations());

            wdDtoList.add(dto);
        }

        return wdDtoList;
    }

    private List<WordEtymologyDto> wordEtyModelToDto(List<WordEtymology> etymologies) {
        List<WordEtymologyDto> etymologyDtos = new ArrayList<>();
        for (WordEtymology etymology : etymologies) {
            WordEtymologyDto etymologyDto = new WordEtymologyDto();
            etymologyDto.setEtymology(etymology.getEtymology());
            etymologyDto.setSource(etymology.getSource());
            etymologyDtos.add(etymologyDto);
        }
        return etymologyDtos;
    }

    private List<WordMeaningDto> wordMeaningModelToDto(List<WordMeaning> meanings) {
        List<WordMeaningDto> meaningsDtos = new ArrayList<>();
        for (WordMeaning wm : meanings) {
            WordMeaningDto wmdto = new WordMeaningDto();
            wmdto.setMeaning(wm.getMeaning());
            wmdto.setPartsOfSpeech(wm.getPartsOfSpeech());
            wmdto.setSource(wm.getSource());
            meaningsDtos.add(wmdto);
        }
        return meaningsDtos;
    }
}
