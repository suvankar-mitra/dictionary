# Dictionary REST API

Dictionary REST API built with `Java` and `Spring Boot`.

REST endpoints:

- `GET` : `/api/v1/random`
  - Success : `200` : Get a random word with its definition as JSON object
  - Not found : `404`
- `GET` : `/api/v1/words/{word}`
  - Success : `200` : Get the definition of the given {word} as JSON object
  - Not found : `404`
